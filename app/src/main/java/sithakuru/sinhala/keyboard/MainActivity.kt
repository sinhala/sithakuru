/*
 * This file is part of Sithakuru.
 *
 * Sithakuru is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * Sithakuru is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Sithakuru.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package sithakuru.sinhala.keyboard

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.core.view.isVisible
import sithakuru.sinhala.keyboard.databinding.ActivityMainBinding
import sithakuru.sinhala.keyboard.databinding.HelpListItemBinding
import sithakuru.sinhala.keyboard.databinding.KeyboardHelpDialogBinding

class MainActivity : Activity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onMenuItemSelected(featureId: Int, item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_about)
            Dialog(this, R.style.Night).let { dialog ->
                dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
                dialog.window?.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
                dialog.window?.setDimAmount(0.8F)
                dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.window?.setGravity(Gravity.CENTER)
                dialog.setContentView(R.layout.about_dialog)
                dialog.setCanceledOnTouchOutside(true)
                dialog.window?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                dialog.findViewById<TextView>(R.id.source_link).setOnClickListener {
                    val intent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.source_link_url))
                    )
                    startActivity(intent)
                }
                dialog.findViewById<TextView>(R.id.license_link).setOnClickListener {
                    val intent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.license_link_url))
                    )
                    startActivity(intent)
                }
                dialog.findViewById<Button>(R.id.dialog_close).setOnClickListener {
                    dialog.dismiss()
                }
                dialog.show()
            }

        return super.onMenuItemSelected(featureId, item)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        actionBar?.title = resources.getString(R.string.app_name_si)

        if (packageName == "kasun.sinhala.keyboard") {
            binding.flavorHelp.isVisible = true
            binding.newFdroidPageButton.setOnClickListener {
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(getString(R.string.fdroid_link_url))
                )
                startActivity(intent)
            }
        } else binding.flavorHelp.isVisible = false

        val prefs = Prefs(this)

        binding.switchEnglish.isChecked = prefs.layoutEnglish
        binding.switchWijesekara.isChecked = prefs.layoutWijesekara
        binding.switchSinglish.isChecked = prefs.layoutSinglish
        binding.switchDark.isChecked = prefs.darkTheme
        binding.switchBorder.isChecked = prefs.keyBorders
        binding.sinhalaLabels.isChecked = prefs.sinhalaKeyLabels
        binding.swipeToErase.isChecked = prefs.swipeToErase
        binding.swipeToMoveCursor.isChecked = prefs.swipeToMoveCursor

        binding.switchEnglish.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked)
                prefs.layoutEnglish = true
            else if (prefs.layoutWijesekara || prefs.layoutSinglish)
                prefs.layoutEnglish = false
            else
                buttonView.isChecked = true
        }
        binding.switchWijesekara.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked)
                prefs.layoutWijesekara = true
            else if (prefs.layoutEnglish || prefs.layoutSinglish)
                prefs.layoutWijesekara = false
            else
                buttonView.isChecked = true
        }
        binding.switchSinglish.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked)
                prefs.layoutSinglish = true
            else if (prefs.layoutEnglish || prefs.layoutWijesekara)
                prefs.layoutSinglish = false
            else
                buttonView.isChecked = true
        }
        binding.switchDark.setOnCheckedChangeListener { _, isChecked -> prefs.darkTheme = isChecked }
        binding.switchBorder.setOnCheckedChangeListener { _, isChecked -> prefs.keyBorders = isChecked }
        binding.sinhalaLabels.setOnCheckedChangeListener { _, isChecked -> prefs.sinhalaKeyLabels = isChecked }

        binding.height.max = 12
        binding.height.progress = (prefs.heightPercentage - KEYBOARD_HEIGHT_VALUE_OFFSET) / KEYBOARD_HEIGHT_STEP_MULTIPLIER
        binding.heightPercentage.text = "${prefs.heightPercentage}%"
        binding.height.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val value = progress * KEYBOARD_HEIGHT_STEP_MULTIPLIER + KEYBOARD_HEIGHT_VALUE_OFFSET
                binding.heightPercentage.text = "$value%"
                prefs.heightPercentage = value
            }
        })

        binding.textSize.max = 20
        binding.textSize.progress = prefs.textSize - TEXT_SIZE_VALUE_OFFSET
        binding.textSizeValue.text = "${prefs.textSize}"
        binding.textSize.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val value = progress + TEXT_SIZE_VALUE_OFFSET
                binding.textSizeValue.text = "$value"
                prefs.textSize = value
            }
        })

        binding.swipeToErase.setOnCheckedChangeListener { _, isChecked -> prefs.swipeToErase = isChecked }
        binding.swipeToMoveCursor.setOnCheckedChangeListener { _, isChecked -> prefs.swipeToMoveCursor = isChecked }

        binding.wijesekaraHelp.setOnClickListener { showHelpDialog(getString(R.string.wijesekara), helpMapWijesekara) }
        binding.singlishHelp.setOnClickListener { showHelpDialog(getString(R.string.singlish), helpMapSinglish) }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        if (hasFocus) {
            var keyboardEnabled = false
            val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            for (i in inputMethodManager.enabledInputMethodList)
                if (i.packageName == BuildConfig.APPLICATION_ID) keyboardEnabled = true

            val keyboardSelected =
                Settings.Secure.getString(contentResolver, Settings.Secure.DEFAULT_INPUT_METHOD)
                    .contains(BuildConfig.APPLICATION_ID)

            if (keyboardEnabled)
                binding.enableHelp.isVisible = false
            else {
                binding.enableHelp.isVisible = true
                binding.enableHelpStep2.text = getString(R.string.enable_help_step_2, getString(R.string.app_name))
            }
            binding.selectHelp.isVisible = keyboardEnabled && !keyboardSelected

            if (!keyboardEnabled)
                binding.enableButton.setOnClickListener { startActivity(Intent(Settings.ACTION_INPUT_METHOD_SETTINGS)) }
            else if (!keyboardSelected)
                binding.selectButton.setOnClickListener { inputMethodManager.showInputMethodPicker() }
        }

        super.onWindowFocusChanged(hasFocus)
    }

    private fun showHelpDialog(keyboardType: String, helpMap: List<Pair<String, String>>) {
        val dialog = Dialog(this)
        val binding = KeyboardHelpDialogBinding.inflate(layoutInflater)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog.window?.setDimAmount(0.8F)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setGravity(Gravity.CENTER)
        dialog.setContentView(binding.root)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        binding.keyboardType.text = keyboardType
        binding.list.adapter = HelpListAdapter(this, helpMap)
        binding.dialogClose.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    override fun onBackPressed() {
        finishAfterTransition()
    }

    private companion object {
        const val KEYBOARD_HEIGHT_VALUE_OFFSET = 70
        const val KEYBOARD_HEIGHT_STEP_MULTIPLIER = 5
        const val TEXT_SIZE_VALUE_OFFSET = 10
    }

    class HelpListAdapter(context: Context, items: List<Pair<String, String>>) :
        ArrayAdapter<Pair<String, String>>(context, R.layout.help_list_item, items) {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val binding = convertView?.let { HelpListItemBinding.bind(it) } ?: HelpListItemBinding.inflate(LayoutInflater.from(context))
            val item = getItem(position)
            binding.first.text = item?.first
            binding.second.text = item?.second
            return binding.root
        }
    }
}
